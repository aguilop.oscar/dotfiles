# Alacritty

![Alacritty](alacritty.png)

1. Instala alacritty.
    - `sudo add-apt-repository ppa:aslatter/ppa && sudo apt install alacritty`
2. Mueve la carpeta `dotfiles/alacritty` al directorio `~/.config/`
