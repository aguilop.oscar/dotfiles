# ZSH

1. Instala zsh
    - `sudo apt install zsh`

    - `sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`
2. Mueve la carpeta `zsh` a `~/.config/`
3. Instala El tema SpaceShip
    - `git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt" --depth=1`
    - `ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"`
4. Crea un enlace simbolico a .zshrc desde el home
    - `rm .zshrc && ln -s .config/zsh/.zshrc .zshrc`
