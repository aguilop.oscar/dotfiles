# Path to your oh-my-zsh installation.
export ZSH="/home/oscar/.oh-my-zsh"

# THEME
ZSH_THEME="spaceship"

SPACESHIP_DIR_TRUNC=2
SPACESHIP_PACKAGE_SHOW=false
SPACESHIP_NODE_SHOW=false

# Plugins
plugins=(git)

source $ZSH/oh-my-zsh.sh

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='nvim'
else
  export EDITOR='nvim'
fi

# Aliases
alias cls="clear"
alias arbol="git log --all --graph --decorate --oneline"
alias nv="nvim"
alias vi="nvim"
alias vim="nvim"
# alias gtop="~/bin/gotop/gotop"

# alias pull="git pull origin master"
# alias push="git push origin master"
