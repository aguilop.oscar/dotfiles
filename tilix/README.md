# TILIX

Para cargar la configuración de tilix nos ayudaremos de dconf, ejecutando el siguiente comando

```dconf load /com/gexperts/Tilix/ < ~/.config/tilix/tilix.dconf```

En caso de querer hacer un respaldo nuevamente, ejecutar el siguiente

```dconf dump /com/gexperts/Tilix/ > ~/.config/tilix/tilix.dconf```
