# XAMPP

1. Descarga XAMPP para Linux desde [aquí](https://www.apachefriends.org/es/index.html)
2. Al terminar la descarga nos queda un archivo .run, que debemos instalar de la siguiente manera:
    - Abrimos una terminal y escribimos lo siguiente
        ~~~
        $ sudo su
        $ chmod +x xampp-linux-x64-5.6.28-0-installer.run
        $ ./xampp-linux-x64-5.6.28-0-installer.run
        ~~~
    - Aceptamos todo y esperamos a que termine la instalación.
3. Procedemos a configurar XAMPP
    - Configuracion de MySQL
        ~~~
        $ sudo ln -s /opt/lampp/bin/mysql /usr/bin/
        $ which mysql
        $ type mysql
        $ ls -lart /usr/bin/mysql
        ~~~

    - Configuración de PHP
        ~~~
        $ sudo ln -s /opt/lampp/bin/php* /usr/bin/
        $ which php
        $ type php
        $ ls -lart /usr/bin/php
        ~~~

    - Configuramos la regla **com.ubuntu.pkexec.xampp.policy** para que el panel grafico 
    ejecute con permisos de root la aplicación, para esto nos dirigimos a la ruta
    **/usr/share/polkit-1/actions** y ejecutamos lo siguiente:
        ~~~
        $ sudo vim com.ubuntu.pkexec.xampp.policy
        ~~~
    Dentro del archivo **com.ubuntu.pkexec.xampp.policy** pegamos el siguiente codigo:
    ~~~
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE policyconfig  PUBLIC
    "-//freedesktop//DTD PolicyKit Policy Configuration 1.0//EN"
    "http://www.freedesktop.org/standards/PolicyKit/1/policyconfig.dtd">
    <policyconfig>

    <action id="com.ubuntu.pkexec.xampp.policy">
        <message>Authentication is required to run XAMP Control Panel</message>
        <icon_name>xampp</icon_name>
        <defaults>
        <allow_any>auth_admin</allow_any>
        <allow_inactive>auth_admin</allow_inactive>
        <allow_active>auth_admin</allow_active>
        </defaults>
        <annotate key="org.freedesktop.policykit.exec.path">/opt/lampp/manager-linux-x64.run</annotate>
        <annotate key="org.freedesktop.policykit.exec.allow_gui">true</annotate>
    </action>

    </policyconfig>
    ~~~

    - Creamos un script para ejecutar el panel gráfico de XAMPP en la ruta **/usr/bin/**
    ~~~
    $ sudo vim xampp-control-panel.sh
    ~~~
    y pegamos el siguiente codigo:
    ~~~
    #!/bin/bash
    $(pkexec /opt/lampp/manager-linux-x64.run);
    ~~~

    - Ahora Creamos el lanzador con alacarte (GNOME)
    ![Configuración de XAMPP en alacarte](./screenshots/lanzador-xampp.png)
    ~~~
    **Name -->** XAMPP Control Panel
    **Command -->** sh /usr/bin/xampp-control-panel.sh
    **Comment -->** Start/Stop XAMPP
    ~~~

    - Por ultimo cambiamos la ruta del directorio **htdocs/**
    ![Configuracion de apache](./screenshots/config-apache.png)
    ![Ruta de htdocs](./screenshots/ruta-htdocs.png)

4. Instala xdebug desde este [enlace](https://xdebug.org/wizard)
5. Instala composer:
    - ```sudo curl -s https://getcomposer.org/installer | /opt/lampp/bin/php```
    - ```sudo ln -s /opt/lampp/bin/php /usr/local/bin/php```
    - ```sudo mv composer.phar /usr/local/bin/composer```