# Dotfiles & Linux Configs

![Screenshot](./screenshots/dotfiles.png)

## Contentenido
1. [Zsh configs](./zsh/) 
2. [Neovim configs](./nvim/)
3. [Alacritty configs](./alacritty/)


*******************************

# Instalación de XAMPP en Ubuntu
- [XAMPP](./apps-install-instrucctions/XAMPP.md)