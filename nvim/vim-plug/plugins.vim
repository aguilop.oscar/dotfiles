" Plugins
call plug#begin('~/.vim/plugged')

" Themes and visual
Plug 'morhetz/gruvbox'
Plug 'bignimbus/pop-punk.vim'
Plug 'folke/tokyonight.nvim'

" Iconos
Plug 'ryanoasis/vim-devicons'

" typing
Plug 'tpope/vim-surround'

" IDE
Plug 'easymotion/vim-easymotion'
Plug 'scrooloose/nerdtree'
Plug 'christoomey/vim-tmux-navigator'
Plug 'vim-scripts/AutoClose'
Plug 'jiangmiao/auto-pairs'
" Plug 'mattn/emmet-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-commentary'
Plug 'turbio/bracey.vim', {'do': 'npm install --prefix server'} " Uncomment if you're not in WSL2
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'prettier/vim-prettier', { 'do': 'npm install' }
Plug 'yggdroot/indentline'
Plug 'honza/vim-snippets'

" PHP Plugins
Plug 'StanAngeloff/php.vim'
" Plug 'stephpy/vim-php-cs-fixer'

" Git integration
Plug 'mhinz/vim-signify'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'junegunn/gv.vim'

call plug#end()


