" Atajos
let mapleader=" "

nmap <Leader>s <Plug>(easymotion-s2)
nmap <Leader>x :x<CR>
nmap <Leader>q :q<CR>
nmap <Leader>qq :q!<CR>
nmap <Leader>w :w<CR>
nmap <Leader>e <C-^>
nmap <Leader>ñ :!
imap jj <Esc>


